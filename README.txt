/*
    Ejercicio de clase. Grado superior (DAM)

    ATENCION:
	Es necesario tener el mysql workbench abierto, no se que porque.
	Me refiero a que yo tengo la base de datos en una maquina virtual
	y necesito tener abierto el cliente de mysql y la base de datos en la maquina virtual

        Las posibles excepciones se controlan a fondo sólo en la tabla products
        En el resto de tablas las excepciones serían controladas de forma similar

        Se adapta una tabla de entidad y una tabla de relacion al mismo objeto 'Order'

        Se pueden buscar clientes por empleados

        Se puede buscar el precio medio de compra de los productos

*/